# test_tv_table

## Project setup (`just run this`)
```
npm i && npm run serve
```

### (`For all next times just run this`)
```
npm run serve
```

---
##### For other stuff  

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
