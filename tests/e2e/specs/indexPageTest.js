// https://docs.cypress.io/api/introduction/api.html

describe("Index page test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("label", "Search");
    cy.request({
      method: "get",
      url: "http://api.tvmaze.com/search/shows?q=girls",
      status: 200
    });
  });
  it("Test search", () => {
    cy.visit("/");
    cy.request({
      method: "get",
      url: "http://api.tvmaze.com/search/shows?q=test",
      status: 200
    }).then(() => {
      cy.get("table").find("td");
    });
  });
});
